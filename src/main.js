import Vue from 'vue';
import VueMaterial from 'vue-material';
import lodash from 'lodash';
import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';
import VueRouter from 'vue-router';
import { routes } from './routes';
import App from './App.vue'
import HomeScreen from "./layouts/HomeScreen.vue";
import noSideNav from "./layouts/noSideNav.vue";
import Main from "./layouts/Main.vue";

Vue.component('Main', Main);
Vue.component('HomeScreen', HomeScreen);
Vue.component('noSideNav', noSideNav);


Vue.use(VueMaterial);
Vue.use(VueRouter);

const router = new VueRouter({
  routes,
})

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
  	next('/404');
  } else {
  	next();
  }

  setTimeout(() => {
    window.scrollTo(0, 0);
  }, 100);
});

const app = new Vue({
  el: '#app',
  data: {
    currentRoute: window.location.pathname,
    menuVisible: false
  },
  router,
  computed: {
    ViewComponent() {
      const matchingView = routes[this.currentRoute]
      return matchingView
        ? require('./pages/' + matchingView + '.vue')
        : require('./layouts/Main.vue')
    }
  },
  render: h => h(App)
})

window.addEventListener('popstate', () => {
  app.currentRoute = window.location.pathname
})
