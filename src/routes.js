import Home from './pages/home.vue';
import Fab from './pages/fab.vue';
import Elevation from './pages/elevation.vue';
import Typography from './pages/typography.vue';
import PageLayout from './pages/page-layout.vue';
import NavigationDrawer from './pages/navigation-drawer.vue';
import Toolbar from './pages/toolbar.vue';
import Errors from './pages/errors.vue';
import Tooltips from './pages/tooltips.vue';
import Buttons from './pages/buttons.vue';
import DataTable from './pages/data-table.vue';
import GuidedConversations from './pages/guided-conversations.vue';
import ProgressIndicators from './pages/progress-indicators.vue';
import ShoppingCart from './pages/shopping-cart.vue';
import TextField from './pages/text-field.vue';
import Browse from './pages/browse.vue';
import Chips from './pages/chips.vue';
import Cards from './pages/cards.vue';
import Stepper from './pages/stepper.vue';
import Dialogs from './pages/dialogs.vue';
import Autocomplete from './pages/autocomplete.vue';
import About from './pages/about.vue';
import FiltersFacets from './pages/filters-facets.vue';
import Icons from './pages/icons.vue';
import DataEntry from './pages/data-entry.vue';
import OverflowMenu from './pages/overflow-menu.vue';
import Tabs from './pages/tabs.vue';
import Calendar from './pages/calendar.vue';
import testPage from './pages/testPage.vue';
import Sidesheets from './pages/sidesheets.vue';
import SelectionControls from './pages/selection-controls.vue';
import EmptyStates from './pages/emptystates.vue';
import NotFound from './layouts/Main.vue';
import FourOhFour from './pages/404.vue';

export const routes = [
  { path: '/', component: Home, meta: { layout: "HomeScreen" } },
  { path: '/floating-action-button', component: Fab },
  { path: '/elevation', component: Elevation },
  { path: '/typography', component: Typography },
  { path: '/page-layout', component: PageLayout },
  { path: '/navigation-drawer', component: NavigationDrawer },
  { path: '/text-field', component: TextField },
  { path: '/toolbar', component: Toolbar },
  { path: '/errors', component: Errors },
  { path: '/tooltips', component: Tooltips },
  { path: '/buttons', component: Buttons },
  { path: '/browse', component: Browse },
  { path: '/chips', component: Chips },
  { path: '/data-table', component: DataTable },
  { path: '/guided-conversations', component: GuidedConversations },
  { path: '/progress-&-indicators', component: ProgressIndicators },
  { path: '/shopping-cart', component: ShoppingCart },
  { path: '/cards', component: Cards },
  { path: '/stepper', component: Stepper },
  { path: '/dialogs', component: Dialogs },
  { path: '/autocomplete', component: Autocomplete },
  { path: '/about', component: About },
  { path: '/filters-&-facets', component: FiltersFacets },
  { path: '/icons', component: Icons },
  { path: '/data-entry', component: DataEntry },
  { path: '/overflow-menu', component: OverflowMenu },
  { path: '/tabs', component: Tabs },
  { path: '/calendar', component: Calendar },
  { path: '/sidesheets', component: Sidesheets },
  { path: '/selection-controls', component: SelectionControls },
  { path: '/emptystates', component: EmptyStates },
  { path: '/testPage', component: testPage, meta: {layout: "noSideNav"} },
];
